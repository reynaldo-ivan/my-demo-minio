package mx.com.nova.minio.services;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.xmlpull.v1.XmlPullParserException;

import io.minio.MinioClient;
import io.minio.ObjectStat;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidArgumentException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.NoResponseException;
import io.minio.messages.Bucket;
import mx.com.nova.minio.models.Archivo;
import mx.com.nova.minio.models.Credencial; 

public interface MinioService {
	 
	
	public boolean existeNodo(String nombreNodo);
	
	public String subirArchivo(String nodo,String ruta);
	
	public List<Bucket> listaNodos();
	
	public ArrayList<Archivo> listaArchivos(String nombre);
	
	public String crearNodo(String nombreNodo);
	
	public String eliminaNodo(String nombreNodo);
	
	public String generaUrl(String nombreNodo,String nombreArchivo, int tiempoVida);
	
	public String generaUrlPut(String nombreNodo,String nombreArchivo, int tiempoVida);
	
	public String crearUrl(String nodo,String ruta,int tiempoVida);
	
	public String eliminaArchivo(String nodo, String nombreArchivo);
	
	public ObjectStat datosArchivo(String nodo, String nombreArchivo);
	
	//Conexión con minio
	
	public Credencial credencialesPorperties();
	
	public MinioClient conexionMinio();
	  
	public Credencial servicioCredenciales(String url);
	
	public Map<String, Object> obtieneObjeto(String mybucket,String myobject);
	
 
	public String setObjeto(String bucket,String nameFile,InputStream bais) throws InvalidKeyException, InvalidBucketNameException, NoSuchAlgorithmException, InsufficientDataException, NoResponseException, ErrorResponseException, InternalException, InvalidArgumentException, IOException, XmlPullParserException;
	
}
