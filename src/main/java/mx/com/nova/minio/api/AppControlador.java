package mx.com.nova.minio.api;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import mx.com.nova.minio.models.Archivo;
import mx.com.nova.minio.services.MinioService; 
 



/**
 * Copyright (c) 2017 Nova Solution Systems S.A. de C.V. Mexico D.F. Todos los
 * derechos reservados.
 * 
 * @author Reynaldo Ivan Martinez Lopez
 *
 *         ESTE SOFTWARE ES INFORMACIÓN CONFIDENCIAL. PROPIEDAD DE NOVA SOLUTION
 *         SYSTEMS. ESTA INFORMACIÓN NO DEBE SER DIVULGADA Y PUEDE SOLAMENTE SER
 *         UTILIZADA DE ACUERDO CON LOS TÉRMINOS DETERMINADOS POR LA EMPRESA SÍ
 *         MISMA.
 */


@RestController
@RequestMapping(value = "/")
public class AppControlador {

	private static final Logger LOGGER = LoggerFactory.getLogger(AppControlador.class);

 
	@Autowired
	private MinioService minio;
	
	 
	 
		@RequestMapping(value = "/subirArchivo", method = RequestMethod.POST, produces = {"application/json"})
		public ResponseEntity<Object> saveFile(MultipartHttpServletRequest multipartHttp, @RequestHeader String bucket) throws Exception {
			 
			System.out.println(bucket);
			Map<String, Object> respuesta = new HashMap<String, Object>();
			
			if (multipartHttp instanceof MultipartHttpServletRequest) {
				
				try{
					MultipartFile multipartFile = multipartHttp.getFile("file"); 
					
					InputStream isInputStream = new ByteArrayInputStream(multipartFile.getBytes());
					
					String rest= minio.setObjeto(bucket,multipartFile.getOriginalFilename(),isInputStream);
					
					respuesta.put("responseStatus", 200);
					respuesta.put("responseError", rest);
					
					return new ResponseEntity<Object>(respuesta, HttpStatus.OK);
				}catch (Exception e) {
					respuesta.put("responseStatus", 400);
					respuesta.put("responseError", e.getMessage());
					return new ResponseEntity<Object>(respuesta, HttpStatus.OK);
				}
				 
		}else{
			respuesta.put("responseStatus", 400);
			respuesta.put("responseError", "ERROR");
			return new ResponseEntity<Object>(respuesta, HttpStatus.OK);
		}

			
	}
	
	
 
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/consulta", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> consulta(RequestEntity<Object> request) {
		LOGGER.info("EndPoint /consulta"); 
		Map<String, Object> sendRequestBody = new HashMap<String, Object>();
		sendRequestBody = (Map<String, Object>) request.getBody();
		Map<String, Object> respuesta = new HashMap<String, Object>();
		 
		try{
			ArrayList<Archivo> respuestaArray=minio.listaArchivos(sendRequestBody.get("bucket").toString());
			return new ResponseEntity<Object>(respuestaArray, HttpStatus.OK);
		}catch (Exception e) {
			respuesta.put("responseStatus", 400);
			respuesta.put("responseError", "ERROR");
			return new ResponseEntity<Object>(respuesta, HttpStatus.OK);
		}

	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/generaUrl", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> generaUrl(RequestEntity<Object> request) {
		
		LOGGER.info("EndPoint /generaUrl");
		Map<String, Object> sendRequestBody = new HashMap<String, Object>();
		sendRequestBody = (Map<String, Object>) request.getBody();
		Map<String, Object> respuesta = new HashMap<String, Object>();
		try{
			String bucket=sendRequestBody.get("bucket").toString();
			String file=sendRequestBody.get("file").toString();
			int time=Integer.parseInt(sendRequestBody.get("time").toString());
		 
			String resultado=minio.generaUrl(bucket,file,time);
			  
				respuesta.put("responseStatus", 200);
				respuesta.put("Url", resultado);
				return new ResponseEntity<Object>(respuesta, HttpStatus.OK);
		}catch (Exception e) {
			respuesta.put("responseStatus", 400);
			respuesta.put("responseError", "ERROR");
			return new ResponseEntity<Object>(respuesta, HttpStatus.OK);
		}
	}
	
	
 
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/obtieneArchivo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = {MediaType.APPLICATION_JSON_VALUE,
																														  MediaType.APPLICATION_XML_VALUE,
																														  MediaType.APPLICATION_OCTET_STREAM_VALUE})
	public ResponseEntity<Object> obtieneArchivo(RequestEntity<Object> request) throws IOException {
		LOGGER.info("EndPoint /obtieneArchivo");
		Map<String, Object> sendRequestBody = new HashMap<String, Object>();
		sendRequestBody = (Map<String, Object>) request.getBody(); 
		 
		Map<String, Object> resulatdo=null;
		try{ 
			resulatdo= new HashMap<String, Object>();
			resulatdo= minio.obtieneObjeto(sendRequestBody.get("bucket").toString(),sendRequestBody.get("file").toString());
			return ResponseEntity
		            .ok()
		            .contentLength(Long.parseLong(resulatdo.get("length").toString()))
		            .contentType(
		                    MediaType.parseMediaType("application/octet-stream"))
		            .body(new InputStreamResource((InputStream) resulatdo.get("stream")));
		}catch (Exception e) {
			resulatdo= new HashMap<String, Object>();
		    resulatdo.put("responseStatus", 400);
			resulatdo.put("responseError", e.getMessage());
			return new ResponseEntity<Object>(resulatdo, HttpStatus.OK);
		}
		
	}
	

}
