# Nombre de microservicio

Microservicio que propociona la funcion de ...
## Uso
 


## Variables de ambiente

Previo a la ejecucion del programa es necesario configurar variables de ambiente


```
	ENDPOINT=http://serviciominio.com
	ACCESSKEY=minio
	SECRETKEY=minio123
```

### Microservicios


```
http://minio.app/obtieneArchivo


- {"bucket":"nova","file":"descarga.jpg"}


http://minio.app/subirArchivo

###Header
- bucket

###Body for-data
- file

http://minio.app/consulta

- {"bucket":"nova"}

http://minio.app/generaUrl

- {"bucket":"nova","file":"descarga.jpg","time":"60"}

```

### Puerto donde se encuentra este microservicio

```
server.port=8080 
```
